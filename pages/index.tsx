import type { NextPage } from 'next'
import Head from 'next/head'
import Container from 'react-bootstrap/Container';
import axios from 'axios'
import Carousel from '../components/caraousel';
import NavbarComponent from '../components/navbarComponent';
import Greeting from '../components/greeting';

const Home: NextPage = ({ genres, movies }: any) => {
  return (
    <div>
      <Head>
        <title>Dadan - SRW Frontend Test</title>
        <meta name="description" content="Test Frontend" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <NavbarComponent />

      <Container className='content'>
        <Greeting />
        {
          genres.genres.map((v: any, k: any) => {
            return (<Carousel movies={movies[k]} genreID={v.name} key={k} title={v.name} />)
          })
        }
      </Container>
    </div >
  )
}

export async function getServerSideProps() {
  //store API key
  const token = '7f753478c8faa75bdd0b9b33b3521125'

  // fetch the genres first (Genres -> Get Movie List). Just use 5 random genres. 
  const genres = await axios.get(`https://api.themoviedb.org/3/genre/movie/list`,
    {
      params: {
        api_key: token
      }
    })
    .then((res) => {
      const shuffled = [...res.data.genres].sort(() => 0.5 - Math.random());
      res.data.genres = shuffled.slice(0, 5);
      return res.data
    })

  //fetch the movies based on those genres
  let movies = [];
  for (let index = 0; index < genres.genres.length; index++) {
    movies[index] = await axios.get(`https://api.themoviedb.org/3/discover/movie`,
      {
        params: {
          api_key: token,
          with_genres: genres.genres[index].id
        }
      })
      .then((res) => {
        return res.data.results
      })
  }

  return {
    props: {
      genres,
      movies
    }
  }
}

export default Home
