import React from 'react'
import { Col, Row } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import styles from './Greeting.module.css'
const Greeting = () => {
    return (
        <Row>
            <Col sm={4}>
                <Card className={styles.card}>
                    <Card.Body>
                        <Card.Title className={styles.cardTitle}>Movies</Card.Title>
                        <Card.Text className={styles.cardSynopsis}>
                            Movies move us like nothing else can, whether they’re scary, funny, dramatic, romantic or anywhere in-between. So many titles, so much to experience.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}

export default Greeting