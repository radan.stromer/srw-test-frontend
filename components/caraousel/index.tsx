import React, { useState } from 'react';
import Image from 'next/image'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import styles from './Carousel.module.css'
import { FiChevronRight, FiChevronLeft } from 'react-icons/fi';
import { Row } from 'react-bootstrap';


const SampleNextArrow = (props: any) => {
    const { className, style, onClick, status } = props;
    return (
        <FiChevronRight onClick={onClick} className={className} style={{ ...style, display: (status) ? "block" : "none", color: 'red' }} />
    );
}

const SamplePrevArrow = (props: any) => {
    const { className, style, onClick, status } = props;
    return (
        <FiChevronLeft onClick={onClick} className={className} style={{ ...style, display: (status) ? "block" : "none", color: 'red' }} />
    );
}


const Carousel = ({ title, movies }: any) => {
    const [prevArrow, setPrevArrow] = useState(false)
    const [nextArrow, setNextArrow] = useState(true)
    const settings = {
        speed: 500,
        slidesToShow: 5.5,
        slidesToScroll: 5,
        swipeToSlide: true,
        arrows: true,
        initialSlide: 0,
        infinite: false,
        nextArrow: <SampleNextArrow status={nextArrow} />,
        prevArrow: <SamplePrevArrow status={prevArrow} />,
        lazyLoad: true,
        beforeChange: (current: any, next: any) => {
            if (next > 1) {
                setPrevArrow(true)
            }
        },
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5.5,
                    slidesToScroll: 5,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    return (
        <Row>
            <span className={styles.titleSection}>{title}</span>
            <Slider {...settings}>
                {movies && movies.map((v: any, k: any) => {
                    return (
                        <div key={k} className={styles.itemslide}>
                            <Image width={350} height={200} src={`https://image.tmdb.org/t/p/w300/${v.backdrop_path}`} />
                            <h6 className={styles.movieTitle}>{v.title}</h6>
                        </div>
                    )
                })}

            </Slider>
        </Row>
    )
}

export default Carousel