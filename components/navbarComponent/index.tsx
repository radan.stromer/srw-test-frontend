import Image from 'next/image'
import { Button } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import styles from './NavbarComponent.module.css'

const NavbarComponent = () => {
    return (
        <Navbar className={styles.navbarComponent} bg="dark" variant="dark">
            <Container>
                <Navbar.Brand href="#">
                    <Image src="/img/logo.png" width={110} height={50} alt="logo" />{' '}
                </Navbar.Brand>
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text className={styles.navbarText}>
                        UNLIMITED TV SHOWS & MOVIES
                    </Navbar.Text>
                    <Button className={styles.joinBtn} variant="danger">JOIN NOW</Button>
                    <Button className={styles.signInBtn} variant="outline-secondary">SIGN IN</Button>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default NavbarComponent